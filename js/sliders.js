$(document).ready(function () {

    
    $("#main-slider").slick({
        prevArrow: "<div class='slick-arrow slick-prev-arrow'></div>",
        nextArrow: "<div class='slick-arrow slick-next-arrow'></div>",
    });
    
   $("#country-slider").slick({
        prevArrow: "<div class='slick-arrow-logo slick-prev-arrow-black'></div>",
        nextArrow: "<div class='slick-arrow-logo slick-next-arrow-black'></div>",
        infinite: true,
        dots: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 700,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 540,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
        ]
    });
    
    $("#reviews-slider").slick({
        prevArrow: "<div class='slick-arrow-logo slick-prev-arrow-black'></div>",
        nextArrow: "<div class='slick-arrow-logo slick-next-arrow-black'></div>",
        infinite: true,
        dots: true,
        slidesToShow: 2,
        slidesToScroll: 2,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1
                }
            }
            
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
        ]
    });


});
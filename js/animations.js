$(document).ready(function () {

	//Класс который должен быть у элемента что бы тот появлялся
	var initClass = "s3-animator-hide";

	//Имя применяемой анимации
	var animatorName = "zoomIn d2";

	var $win = $(window);

	$("." + initClass).each(function(index, element){
  		var $element = $(element);
  		$element.attr('data-s3-animator', animatorName);

  		$win.scroll(function() {
		  	//Складываем значение прокрутки страницы и высоту окна, этим мы получаем положение страницы относительно нижней границы окна, потом проверяем, 
		  	//если это значение больше, чем отступ нужного элемента от верха страницы, то значит элемент уже появился внизу окна, соответственно виден
		  	if($win.scrollTop() + $win.height() >= $element.offset().top) {
		  		//выполняем действия если элемент виден
		   		$element.removeClass("s3-animator-hide").addClass("s3-animator-zoomIn s3-animator-d2 s3-animator");
		  	} else {
		    	//выполняем действия если не элемент виден
		  	}
		});

	});

});
